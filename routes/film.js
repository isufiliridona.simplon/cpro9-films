const express = require('express')
const FilmRouter = express.Router()
const films = require('../films.json')

FilmRouter.get('/all', (req, res) => {
    res.json(films)
})

module.exports = FilmRouter