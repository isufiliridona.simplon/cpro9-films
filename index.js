
const express = require('express')
const app = express()
// const port = 8000;
const FilmRouter = require('./routes/film')


app.get('/home',(req, res) => {
    res.send('Hello Homepage')
})


app.use('/films', FilmRouter)

app.get('/films/:id', (req, res) => {
    res.json(req.params.id)
})

// app.post('/new', (req, res) => {

// })

app.listen(4567, () => {
    console.log(`[mon petit server]`)
})